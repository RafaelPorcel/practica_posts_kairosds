/// <reference types="Cypress" />

it('user click on Add button, change to mode "add", user click add for add post', () => {
    cy.visit('/posts')
    cy.get('posts-page-genk').shadow()
        .find('.list')
        .find('.listPost')
        .shadow()
        .find('.add_button')
        .click()

    cy.get('posts-page-genk').shadow()
        .find('.modeAdd')
        .shadow()
        .find('.titleInput')
        .clear().type('NUEVO POST - TITULO', { force: true })

    cy.get('posts-page-genk').shadow()
        .find('.modeAdd')
        .shadow()
        .find('.bodyInput')
        .clear().type('NUEVO POST - BODY', { force: true })

    cy.get('posts-page-genk').shadow()
        .find('.modeAdd')
        .shadow()
        .find('.add_button')
        .click()

    cy.get('posts-page-genk').shadow()
        .find('.list')
        .find('.listPost')
        .shadow()
        .find('#post_101').should('exist')

    cy.get('posts-page-genk').shadow()
        .find('.list')
        .find('.listPost')
        .shadow()
        .find('#post_101')
        .click()

    cy.get('posts-page-genk').shadow()
        .find('.list')
        .find('.listPost')
        .shadow()
        .find('#post_101')
        .click()

    cy.get('posts-page-genk').shadow()
        .find('.details')
        .shadow()
        .find('.titleInput').should('have.value', 'NUEVO POST - TITULO')
        
    cy.get('posts-page-genk').shadow()
        .find('.details')
        .shadow()
        .find('.bodyInput').should('have.value', 'NUEVO POST - BODY')
})
