/// <reference types="Cypress" />

it('click on post - event clickedPost', (() => {

    cy.visit('/posts')
    cy.get('posts-page-genk').shadow()
        .find('.list')
        .find('.listPost')
        .shadow()
        .find('.post')
        .first()
        .click()
    cy.get('posts-page-genk').shadow()
        .find('.details')
        .shadow()
        .find('.titleInput').should('have.value', 'sunt aut facere repellat provident occaecati excepturi optio reprehenderit')
    cy.get('posts-page-genk').shadow()
        .find('.details')
        .shadow()
        .find('.bodyInput').should('have.value', 'quia et suscipitsuscipit recusandae consequuntur expedita et cumreprehenderit ' 
        +'molestiae ut ut quas totamnostrum rerum est autem sunt rem eveniet architecto')

    cy.get('posts-page-genk').shadow()
        .find('.list')
        .find('.listPost')
        .shadow()
        .find('.post:nth-child(2)')
        .first()
        .click()
    cy.get('posts-page-genk').shadow()
        .find('.details')
        .shadow()
        .find('.titleInput').should('have.value', 'qui est esse')
    cy.get('posts-page-genk').shadow()
        .find('.details')
        .shadow()
        .find('.bodyInput').should('have.value', 'est rerum tempore vitaesequi sint nihil reprehenderit dolor beatae ea dolores'
        +' nequefugiat blanditiis voluptate porro vel nihil molestiae ut reiciendisqui aperiam non debitis possimus qui neque nisi nulla')
}))

