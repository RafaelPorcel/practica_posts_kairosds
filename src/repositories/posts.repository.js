import axios from "axios";

export class PostsRepository {
    async getAllPosts() {
        return await (
            await axios.get('https://jsonplaceholder.typicode.com/posts')
        ).data;
    }

    async deletePost(id) {
        return (
            await axios.delete(`https://jsonplaceholder.typicode.com/posts/${id}`)
        ).data;
    }

    async updatePost(id, title, body) {
        return (
            await axios.put(
                `https://jsonplaceholder.typicode.com/posts/${id}`,
                { title, body }
            )
        ).data;
    }

    async addPost(title, body) {
        return (
            await axios.post('https://jsonplaceholder.typicode.com/posts',
                { title, body }
            )
        ).data
    }
}
