import { css, html, LitElement } from "lit";

export class PostsUI extends LitElement {

    static get properties() {
        return {
            posts: {
                type: Array
            },
            showAddComponent: {
                type: Boolean
            }
        }
    }

    static get styles() {
        return css`
            .posts_container {
                border: black solid 5px;
                width: 100%;
                background-color: aliceblue;
                border-radius: 20px;
            }
            h3{
                margin-left: 20px;
                width: 20%;
                border: rgb(85, 132, 163) solid 3px;
            }
            .add_button{
                margin-left: 20px;
            }
            .post:hover{
                background-color: rgb(85, 132, 163);
            }
        `
    }

    clickPost(event) {
        const postElement = event.target;//se obtiene el elemento clicado
        const postId = postElement.id.split("_")[1];//se obtine el id del post clicado
        const post = this.posts.find((post) => post.id === Number(postId));//se compara con el post de la lista a traves de su id y se obtiene el post
        this.dispatchEvent(new CustomEvent('clickedPost', {//lanzo un nuevo objeto CustomEvent. el nombre del evento a capturar en el otro componente es clickedPost
            detail: {//detalles del post seleccionado
                title: post.title,
                body: post.body,
                id: post.id
            },
            bubbles: true,
            composed: true
        }));
    }

    clickAdd() {
        this.showAddComponent = true;
        
        this.dispatchEvent(new CustomEvent('clickAdd', {
            detail: {
                showAddComponent: this.showAddComponent
            },
            bubbles: true,
            composed: true
        }));
    }

    render() {
        console.log('RENDER DE posts.ui', this.posts);

        return html`
            <div class="posts_container">
                <h3>POSTS LIST</h3>
                <button class="add_button" 
                    @click="${this.clickAdd}">ADD
                </button>
                <ul id="posts">
                ${this.posts && this.posts.map((post) => html`
                    <li class="post" id="post_${post.id}" 
                        @click="${this.clickPost}">
                        ${post.id} -- ${post.title}
                    </li>
                `)}
                </ul>
            </div>
        `;
    }
}

customElements.define('posts-ui', PostsUI);