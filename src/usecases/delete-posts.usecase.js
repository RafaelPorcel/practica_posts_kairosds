import { PostsRepository } from "../repositories/posts.repository";

export class DeletePostUseCase {
    async execute(postsList, id) {
        const repository = new PostsRepository();
        await repository.deletePost(id);

        return postsList.filter((post) => post.id !== Number(id));
    }
}
