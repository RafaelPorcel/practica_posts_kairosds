import { PostsRepository } from "../repositories/posts.repository";
import { UtilsService } from "../services/utils.services";

export class OddPostsUseCase {

    async execute(postsList) {
        const repository = new PostsRepository(postsList);
        const posts = await repository.getAllPosts();
        const oddPosts = posts.filter((post) => UtilsService.isOdd(post.id));
        return oddPosts;
    }

}