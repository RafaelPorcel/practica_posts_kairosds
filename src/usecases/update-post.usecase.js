import { PostsRepository } from "../repositories/posts.repository";

export class UpdatePostUseCase {

  async execute(postsList, id, title, body) {
    const repository = new PostsRepository();
    await repository.updatePost(id, { title, body });

    const updatedPosts = { id, title, body };

    return postsList.map(post => {
      if (post.id === id) {
        return { id, title, body };
      }
      return post;
    });

  }
}

