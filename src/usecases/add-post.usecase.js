import { PostsRepository } from "../repositories/posts.repository";

export class AddPostUseCase {

    async execute(postsList, title, body) {
        const repository = new PostsRepository();
        const posts = await repository.addPost(title, body);

        const lastId = postsList.reduce((accumulator, post) => Math.max(accumulator, post.id), 0) + 1;

        const newPost = {

            id: lastId,
            title: title,
            body: body
        };
        return postsList.concat(newPost);
    }
}