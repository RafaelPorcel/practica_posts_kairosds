import { LitElement, html, css } from 'lit';
import { PostsListComponent } from '../components/postsList.component';
import { PostsDetailsComponent } from '../components/postsDetails.component';
import { PostAddComponent } from '../components/postAdd.component';

export class PostsPage extends LitElement {

    static get properties() {
        return {
            showAddComponent: {
                type: Boolean
            }
        }
    }

    connectedCallback() {
        super.connectedCallback();
        this.showAddComponent = true;
        document.addEventListener('clickAdd', this.handleAddButton.bind(this));
        document.addEventListener('clickedPost', this.handleClikedPostEvent.bind(this));
    }

    handleAddButton(event) {
        this.showAddComponent = false;
    }

    handleClikedPostEvent(){
        this.showAddComponent = true;
    }


    static get styles() {
        return css`
            .super_container {
                display: flex;
                align-items: flex-start;
            }
            .list {
                width: 60%;
                margin-top: 30px;
            }
        `
    }

    render() {
        var selectedComponent;
        if (this.showAddComponent === true) {
            selectedComponent = html`<posts-details-component class="details">
            </posts-details-component>`;
        } else if (this.showAddComponent === false) {
            selectedComponent = html`<posts-add-component class="modeAdd"></posts-add-component>`;
        }

        return html`
          <div class="super_container">
            <genk-posts class="list"></genk-posts>
            ${selectedComponent}
          </div>
        `;
    }
}

customElements.define('posts-page-genk', PostsPage);
