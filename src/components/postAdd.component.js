import { LitElement, html, css } from 'lit';
import { AddPostUseCase } from "../usecases/add-post.usecase";
import { state } from '../states/state';


class PostsAddComponent extends LitElement {
  static get properties() {
    return {
      title: {
        type: String
      },
      body: {
        type: String
      },
      id: {
        type: Number
      }
    };
  }

  connectedCallback() {
    super.connectedCallback();
    this.title = 'New title';
    this.body = 'New body';
  }

  static get styles() {
    return css`
      .container {
        border: 5px solid black;
        padding: 10px;
        width: 100%;
        margin: 50px;
        display: flex;
        flex-direction: column;
        border-radius: 20px;
        background-color: aliceblue;
      }
      .title {
        justify-content: left;
      }
      .inputs > div {
        align-items: center;
        margin-bottom: 10px;
      }
      input {
        width: 100%
      }
      .buttons {        
        justify-content: left;
        margin-top: 20px;
      }
      h3{
        width: 50%;
        border: rgb(85, 132, 163) solid 3px;
      }
    `;
  }

  render() {
    return html`
      <div class="container">
        <h3 class="title">POSTS ADD</h3>
        <div class="inputs">
          <div>
            <div>Title:</div>
            <input type="text" id="titleInput" class="titleInput" .value="${this.title}">
          </div>
          <div>
            <div>Body:</div>
            <input type="text" id="bodyInput" class="bodyInput" .value="${this.body}">
          </div>
        </div>
        <div class="buttons">
            <button class="add_button"
                @click="${this.clickAdd}">Add
            </button>
            <button class="cancel_button"
                @click="${this.clickCancel}">Cancel
            </button>
        </div>
      </div>
    `;
  }

  clickCancel() {
    this.shadowRoot.getElementById('titleInput').value = 'New title';
    this.shadowRoot.getElementById('bodyInput').value = 'New body';
  }

  async clickAdd() {
    this.title = this.shadowRoot.getElementById('titleInput').value;
    this.body = this.shadowRoot.getElementById('bodyInput').value;

    const addPostUseCase = new AddPostUseCase();
    const updatedListPost = await addPostUseCase.execute(state.posts, this.title, this.body);

    this.dispatchEvent(new CustomEvent('postAdded', {
      detail: {
        updatedListPost: updatedListPost
      },
      bubbles: true,
      composed: true
    }));

    this.shadowRoot.getElementById('titleInput').value = 'New title';
    this.shadowRoot.getElementById('bodyInput').value = 'New body';
  }

}

customElements.define('posts-add-component', PostsAddComponent);