import { LitElement, html, css } from 'lit';
import { DeletePostUseCase } from "../usecases/delete-posts.usecase";
import { UpdatePostUseCase } from "../usecases/update-post.usecase";
import { state } from '../states/state';


export class PostsDetailsComponent extends LitElement {
  static get properties() {
    return {
      title: {
        type: String
      },
      body: {
        type: String
      },
      id: {
        type: Number
      }
    };
  }

  connectedCallback() {
    super.connectedCallback();
    this.title = 'Click on post';
    this.body = 'Click on post';
    document.addEventListener('clickedPost', this.handleClikedPostEvent.bind(this));
    //escucho el evento clickedPost de mi otro componente y utilizo como manejador handleClikedPostEvent
  }

  handleClikedPostEvent(event) {//manejador del evento que escucho cuando hago click en un post dle otro componente PostUI
    const { title, body, id } = event.detail;
    //aquí le doy el valor de las propiedades del otro componente a este componente
    this.title = title;
    this.body = body;
    this.id = id;
  }

  static get styles() {
    return css`
      .container {
        border: 5px solid black;
        padding: 10px;
        width: 100%;
        margin: 50px;
        display: flex;
        flex-direction: column;
        border-radius: 20px;
        background-color: aliceblue;
      }
      .title {
        justify-content: left;
      }
      .inputs > div {
        align-items: center;
        margin-bottom: 10px;
      }
      input {
        width: 100%
      }
      .buttons {        
        justify-content: left;
        margin-top: 20px;
      }
      h3{
        width: 60%;
        border: rgb(85, 132, 163) solid 3px;
      }
    `;
  }

  render() {
    return html`
      <div class="container">
        <h3 class="title">POSTS DETAILS</h3>
        <div class="inputs">
          <div>
            <div>Title:</div>
            <input type="text" id="titleInput" class="titleInput" .value="${this.title}">
          </div>
          <div>
            <div>Body:</div>
            <input type="text" id="bodyInput" class="bodyInput" .value="${this.body}">
          </div>
        </div>
        <div class="buttons">
          <button class="cancel_button"
            @click="${this.clickCancel}">Cancel
          </button>
          <button class="update_button"
            @click="${this.clickUpdate}">Update
          </button>
          <button class="delete_button"
            @click="${this.clickDelete}">Delete
            </button>
        </div>
      </div>
    `;
  }

  async clickUpdate() {
    this.title = this.shadowRoot.getElementById('titleInput').value;
    this.body = this.shadowRoot.getElementById('bodyInput').value;

    const updatePostUseCase = new UpdatePostUseCase();
    const updatedListPost = await updatePostUseCase.execute(state.posts, this.id, this.title, this.body);

    this.dispatchEvent(new CustomEvent('postUpdated', {
      detail: {
        updatedListPost :  updatedListPost
      },
      bubbles: true,
      composed: true
    }));
    this.title = "Click on post";
    this.body = "Click on post";
  }

  async clickDelete() {
    const deletePostUseCase = new DeletePostUseCase();
    const updatedListPost = await deletePostUseCase.execute(state.posts, this.id);

    this.dispatchEvent(new CustomEvent('postDeleted', {
      detail: {
        updatedListPost :  updatedListPost
      },
      bubbles: true,
      composed: true
    }));

    this.title = "Click on post";
    this.body = "Click on post";
  }
  
  clickCancel() {
    this.title = "Click on post";
    this.body = "Click on post";
  }
}

customElements.define('posts-details-component', PostsDetailsComponent);