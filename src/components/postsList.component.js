import { html, LitElement } from "lit";
import { AllPostsUseCase } from "../usecases/all-posts.usecase";
import { OddPostsUseCase } from "../usecases/odd-posts.usecase";

import './../ui/posts.ui';

import { state } from '../states/state';

export class PostsListComponent extends LitElement {

    static get properties() {
        return {
            posts: {
                type: Array,
                state: true
            }
        }
    }

    async connectedCallback() {
        super.connectedCallback();
        const allPostsUseCase = new AllPostsUseCase();
        this.posts = await allPostsUseCase.execute();
        state.posts = this.posts;
        document.addEventListener('postDeleted', this.handleDeletePost.bind(this));
        document.addEventListener('postUpdated', this.handleUpdatePost.bind(this));
        document.addEventListener('postAdded', this.handleAddPost.bind(this));
    }

    async allOdds() {
        const oddPostsUseCase = new OddPostsUseCase();
        this.posts = await oddPostsUseCase.execute(state.posts);
    }

    async handleDeletePost(event) {
        this.posts =  event.detail.updatedListPost;
        state.posts = this.posts;
    }

    async handleUpdatePost(event) {
        this.posts =  event.detail.updatedListPost;
        state.posts = this.posts;
    }

    async handleAddPost(event) {
        this.posts =  event.detail.updatedListPost;
        state.posts = this.posts;
    }

    render() {
        return html`
            <button @click="${this.allOdds}" id="oddAction">Odd</button>
            <posts-ui class="listPost" .posts="${this.posts}"></posts-ui>
        `;
    }

    createRenderRoot() {
        return this;
    }
}

customElements.define('genk-posts', PostsListComponent);