import { OddPostsUseCase } from '../../src/usecases/odd-posts.usecase';
import  POSTS  from '../../fixtures/posts.json';

describe.only('Odd posts usecase', () => {

    it('should execute correct', async () => {

        const postsList = POSTS;
        const useCase = new OddPostsUseCase();
        const modifiedPostList = await useCase.execute(postsList);

        expect(modifiedPostList.map(post => post.id)).not.toContain(2);
        expect(modifiedPostList).toHaveLength(50);
    })

})
