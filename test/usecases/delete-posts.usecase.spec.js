import { DeletePostUseCase } from '../../src/usecases/delete-posts.usecase';
import  POSTS  from '../../fixtures/posts.json';

describe.only('Delete posts usecase', () => {

    it('should execute correct', async () => {

        const id = 1;
        const postsList = POSTS;
        const useCase = new DeletePostUseCase();
        const modifiedPostList = await useCase.execute(postsList, id);

        expect(modifiedPostList).toHaveLength(99);
    })

})
