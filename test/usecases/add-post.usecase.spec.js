import { AddPostUseCase } from '../../src/usecases/add-post.usecase';
import  POSTS  from '../../fixtures/posts.json';

describe('Add posts usecase', () => {

    it('should execute correct', async () => {

        const newPost = {id:101, title:'title 1 new', body:'body 1 new'};
        const postsList = POSTS;
        const useCase = new AddPostUseCase();
        const modifiedPostsList = await useCase.execute(postsList, newPost.title, newPost.body);

        
        expect(modifiedPostsList).toHaveLength(101);
        expect(modifiedPostsList[100]).toEqual(newPost);
    })

})
