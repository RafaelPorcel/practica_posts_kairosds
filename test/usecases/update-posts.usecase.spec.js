import { UpdatePostUseCase } from '../../src/usecases/update-post.usecase';
import  POSTS  from '../../fixtures/posts.json';

describe('Update posts usecase', () => {

    it('should execute correct', async () => {

        const postUpdated = {id: 1 , title:'title 1 modified', body:'body 1 modified'};
        const postsList = POSTS;
        const useCase = new UpdatePostUseCase();
        const updatedPostsList = await useCase.execute(postsList, postUpdated.id, postUpdated.title, postUpdated.body);


        expect(updatedPostsList[0]).toEqual(postUpdated);
    })

})
